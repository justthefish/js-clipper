<form class="form form-horizontal" name="edit-form" action="" method="post">
        <fieldset>
            <legend>Edit/add container</legend>
            <div class="control-group">
                <label class="control-label">
                    Campaign ID
                </label>
                <div class="controls">
                <input class="input-large" type="text" name="campaign" value="<?php echo (isset($snippet['campaign'])?$snippet['campaign']:'');?>"/>
                </div>
            </div>
            
            <?php $count = 0; ?> 
            <?php if (isset($snippet["snippets"]) && is_array($snippet["snippets"]) ) {
                    foreach ($snippet["snippets"] as $id => $txt) {?>
            <span class="block"> 
            <hr/>
            <div class="control-group">
                <label class="control-label">
                    SSP name: 
                </label>
                <div class="controls">
                <input class="input-sm span10" type="text" name="snippets[<?php echo $count;?>][id]" value="<?php echo $id; ?>">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">
                    code: 
                </label>
                <div class="controls">
                <textarea class="codeTA span10" name="snippets[<?php echo $count;?>][txt]"><?php echo $txt; ?></textarea>
                </div>
            </div>
            </span>
            <?php 
                $count++;
            }}?>
            <span class="block">
            <hr/>
            <div class="control-group">
                <label class="control-label">
                    SSP name: 
                </label>
                <div class="controls">
                    <input class="input-sm span10" type="text" name="snippets[<?php echo $count;?>][id]" value="">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">
                    code: 
                </label>
                <div class="controls">
                    <textarea class="codeTA span10" name="snippets[<?php echo $count;?>][txt]"></textarea>
                </div>
            </div>
            </span>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-primary" type="submit">save</button>
                    <a class="addmore btn">Добавить SSP</a>
                </div>
            </div>
        
        </fieldset>
</form>
<script>
    $(document).ready(function(){
        $('a.addmore').click(function(e) {
            e.preventDefault();
            $('span.block:last').clone().insertAfter('span.block:last');
            $('span.block:last').find(':input').each(function (idx, elt){
                $(elt).attr(
                    'name', 
                    function (txt) {
                        return (txt.replace(/\[(\d+)\]/, function (full, num) {
                            return '[' + parseInt( parseInt(num) + 1) + ']';
                        } ));
                    }( 
                        $(elt).attr('name') 
                    )
                );
            });
        });
    });
</script>
