<form class="form form-horizontal" name="login-form" action="" method="post">
        <fieldset>
            <legend>Register</legend>
            <?php if (!empty($errors)) { ?>
                <div class="alert alert-block alert-warning">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        <?php foreach ($errors as $err) echo '<li>'.$err.'</li>';?>
                    </ul>
                </div>
            <?php } ?>
            <div class="control-group">
                <label class="control-label">
                    login
                </label>
                <div class="controls">
                <input class="input-sm" type="text" name="login" value="<?php echo $user->login?>">
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label">
                    email
                </label>
                <div class="controls">
                <input class="input-sm" type="text" name="email" value="<?php echo $user->email?>">
                </div>
            </div>
            
    
            <div class="control-group">
                <label class="control-label">
                    pass
                </label>
                <div class="controls">
                    <input class="input-sm" type="password" name="password" value="">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-primary" type="submit">Register!</button>
                    <a class="btn" href="/account/login">...or login.</a></td>
                </div>
            </div>
        
        </fieldset>
</form>
