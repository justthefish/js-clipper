<form class="form form-horizontal" name="login-form" action="" method="post">
        <fieldset>
            <legend>Login</legend>
            <?php if (isset($errors)) { ?>
                <div class="alert alert-block alert-warning">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        <?php foreach ($errors as $err) echo '<li>'.$err.'</li>';?>
                    </ul>
                </div>
            <?php } ?>
            <div class="control-group">
                <label class="control-label">
                    email
                </label>
                <div class="controls">
                <input class="input-sm" type="text" name="email" value="<?php echo (isset($email)?$email:'');?>"/>
                </div>
            </div>
            
    
            <div class="control-group">
                <label class="control-label">
                    pass
                </label>
                <div class="controls">
                    <input class="input-sm" type="password" name="password" value="">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-primary" type="submit">Log in!</button>
                    <a class="btn" href="/account/register">No account?</a></td>
                </div>
            </div>
        
        </fieldset>
</form>
