<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * account contoller
 **/
class Account_Controller extends Controller {
    
    public function action_index() {
        $this->request->redirect('/account/login');
    }

    public function action_login() {
        if ($this->auth->loggedIn()) {
            $this->request->redirect(BASE_URL);
        }
        $view = View::factory('account/login');
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if($this->auth->login($_POST["email"], $_POST["password"], true)) {
                $this->request->redirect('/'); //redirect to root
            } else {
                $view->bind('email', $_POST["email"])->set('errors', array('Wrong username or password'));
            }
        }
        $this->template->content = $view;
    }

    public function action_logout() {
        $this->auth->logout();
        $this->request->redirect('/');
    }

    public function action_register() {
        $user = new User();
        $errors = $arr = array();
        if ($_SERVER["REQUEST_METHOD"] == 'POST') {

            $arr['login'] = trim($_POST['login']);
            $arr['password'] = (empty($_POST['password']) ? null : $this->auth->hash(trim($_POST['password'])));
            $arr['email'] = trim($_POST['email']);

            $arr = array_filter($arr);
            $user->import($arr);
            $errors = $user->errors;

            if (!isset($errors['email']) && $user->getByEmail($arr['email'])) {
                $errors[] = 'Email taken, sorry.';
            }
            if (isset($errors['login'])) {
                $errors[] = 'Login taken. sorry.';
            }

            if (empty ($errors)) {
                $user->save();
                $this->auth->forceLogin($user->email);
                $this->request->redirect('/');
            }
        }

        $this->template->content = View::factory('account/register')
            ->set('user', $user)
            ->set('errors', $errors);
    }
}
