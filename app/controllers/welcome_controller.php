<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * 
 **/
class Welcome_Controller extends Controller {
    
    public function action_index() {

        if (!$this->auth->loggedIn()) {
            $this->template->content = View::factory('page/welcome');
        } else {
            $this->request->redirect('/snippets');
        }
    }
}
