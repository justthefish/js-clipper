<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * account contoller
 **/
class Snippets_Controller extends Controller {

    public function action_index() {
        if (!$this->auth->loggedIn()) {
            $this->request->redirect('/account/login');
        }
        $snippets = new Snippets; 
        $this->template->content = View::factory('snippets/list')
            ->bind('list', $snippets->getList() )
            ->set('user', $this->auth->getUser());
    }

    public function action_edit() {

        if (!$this->auth->loggedIn()) {
            $this->request->redirect('/account/login');
        }

        $snippets = new Snippets;
        
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $id = $this->request->param('id');
            if ($id) {
                $snippet = $snippets->table->findOne(array('_id' => new MongoId($id)));
            } else {
                $snippet = array();
            }
            $this->template->content = View::factory('snippets/edit')
                ->set('snippet', $snippet);
        } else {
            //saving
            $to_save = array();
            if ($this->request->param('id') ) {
                $to_save['_id'] = new MongoId ($this->request->param('id'));
            }
            $to_save['campaign'] = $_POST['campaign'];
            foreach($_POST["snippets"] as $arr) {
                if (empty($arr['id']) || empty($arr['txt'])) {
                    continue;
                }
                $snips[$arr['id']] = $arr['txt'];
            }

            $to_save['snippets'] = $snips;
            $to_save['script'] = $this->writeFiles($to_save);
            $snippets->table->save($to_save);

            $this->request->redirect('/snippets');
        }
    }

    public function action_delete() {
        $snippets = new Snippets;
        $id = $this->request->param('id');
        if ($id) {
            $arr = $snippets->table->findOne(array('_id' => new MongoId($id)));
            try {
            unlink( OUT_FILES_PATH.$arr["campaign"].'.js');
            unlink( OUT_FILES_PATH.$arr["campaign"].'.html');
            } catch (Exception $e) {
                throw $e;
                die();
            }
            $snippets->table->remove(array('_id' => new MongoId($id)));
        }
        $this->request->redirect('/snippets');
    }

    private function writeFiles($arr) {
        $out = "";
        if (!isset($arr['snippets']) ||  !is_array($arr['snippets']) || !$arr['campaign']) {
            return false;
        }

        foreach ($arr['snippets'] as $id => $code) {
            $out .= "\n\n".$code."\n";
        }
        try {
            $path = OUT_FILES_PATH.$arr["campaign"].'.html';

            $html_url = OUT_FILES_URL.$arr["campaign"].'.html';

            file_put_contents($path, $out);

            $path = OUT_FILES_PATH.$arr["campaign"].'.js';
            $js_url = OUT_FILES_URL.$arr["campaign"].'.js';
            $jstext = $this->getJsText($arr, $html_url);
            file_put_contents($path, $jstext);
        } catch (Exception $e) {
            throw $e;
            return false;
        }
        return '<script type="text/javascript" charset="utf-8" src="'.$js_url.'"></script>';
    }


     public function getJsText($arr, $url) {
        $template = "
 function loadAndWrite() {
   var req = false;
   if (window.XMLHttpRequest) {
     try {
       req = new XMLHttpRequest();
     } catch (e) {
       req = false;
     }
   } else if (window.ActiveXObject) {
     try {
       req = new ActiveXObject('Msxml2.XMLHTTP');
     } catch (e) {
       try {
         req = new ActiveXObject('Microsoft.XMLHTTP');
       } catch (e) {
         req = false;
       }
     }
   }
if (req) {
 req.open('GET', '<url>', false);
 req.send(null);
 document.write(req.responseText);
}}
loadAndWrite();
    ";
    return str_replace('<url>', $url, $template);
     }
}
