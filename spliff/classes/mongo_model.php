<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * mongo model
 **/
class Mongo_Model extends Model {

    public $driver = 'mongo';
    //actually, $this->table must contain db.collection

    public function load($id) {
        $this->data = DB::me()
            ->{$this->table}
            ->find(array('_id' =>$id ));
        return $this;
    }

    public function update($options = null) {
        if (is_null($options)) {
            $options = array();
        }
        $model = $this;

        //@todo fix hardcode!
        $identifier = $this->data['_id'];

        $result = DB::me()
            ->{$this->table}
            ->update(
                array('_id' => $identifier),
                $model->data,
                $options
            );
        if ($result) {
            return $model;
        } else {
            throw new Exception("DB update error!");
        }
    }

    public function upsert() {
        return $this->update(array(
            'upsert' => true,
        ));
    }

    public function insert() {
        $model = $this; 
        $result = DB::me()->{$this->table}->insert($model->data);
        //@todo FIX HARDCODE
        return $result['_id'];
    }

    public function save() {
        
        $model = $this;
        foreach ($this->fields as $field => $meta) {
            if ($meta['type'] == 'identifier' && isset($model->{$field})) {
                return $this->update();
            }
        }
        return $this->load($this->insert());
    }

    
    public function import($arr) {

        if (!is_array($arr)) {
            return null;
        }
        $this->data = $arr;
        /*
        foreach ($this->fields as $field => $meta) {
            if (isset($arr[$field])) {
                if ( isset($this->fields[$field]['validate']) ) {
                    $this->data[$field] = $this->{$field} = $this->validate($arr[$field], $field);
                } else {
                    $this->data[$field] = $this->{$field} = $arr[$field];
                }
            } elseif(isset($this->fields[$field]['required'])) {
                $this->errors[$field] = "$field must be set!";
            }
        }
        */
        return $this;
    }
    public function as_array() {
        return $this->data;
    }
}
