<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * request class
 **/
class Request {
    
    public static $instance;
    public static $current;
    public $controller;
    public $action;
    protected $_params = array();

    function __construct() {
        $uri = $this->detect_uri();

        //Crude parsing
        $combine = parse_url($uri);
        $c = explode('/', $combine['path']);
        //hardcode for folder
        array_shift($c);

        $this->controller = empty($c[0]) ? 'welcome' : $c[0];
        $this->action = empty($c[1]) ? 'index' : $c[1];
        $this->_params['id'] = empty($c[2]) ? null : $c[2];
        $param_parser = function() use ($combine) {
            $params = array();
            if (!isset($combine['query'])) return $params;
            foreach(explode('&', $combine['query']) as $pair) {
                $p = explode('=', $pair);
                if (isset($p[1])) {
                    $params[$p[0]] = $p[1];
                }
            }
            return $params;
        };
        $this->_params = array_merge((array)$this->_params, (array)$param_parser());
        //end crude parsing
    }

    public static function me() {
    
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function execute() {
        $postfix = "_controller";
        $previous = Request::$current; //save request
        Request::$current = $this;
        try {
            $class = new ReflectionClass($this->controller.$postfix);
            $controller = $class->newInstance($this);
            $class->getMethod('before')->invoke($controller);
            $action = empty($this->action) ? Route::default_action : $this->action;
            $class->getMethod('action_'.$this->action)->invokeArgs($controller, $this->_params);
            $class->getMethod('after')->invoke($controller);
        } catch (Exception $e) {
            Request::$current = $previous; //возвращаем реквест обратно
            //сюда можно поставить кастомные exceptions
            throw $e;
        }
    }

    public function detect_uri() {

        if (isset($_SERVER["REQUEST_URI"])) {
            $uri = $_SERVER["REQUEST_URI"];
            $uri = rawurldecode($uri);
        } elseif (isset($_SERVER["PHP_SELF"])) {
            $uri = $_SERVER["PHP_SELF"];
        } elseif (isset($_SERVER["REDIRECT_URL"])) {
            $uri = $_SERVER["REDIRECT_URL"];
        } else {
            throw new Exception('Unable to detect uri. Are we even using server?');
        }
        return $uri;
    }

    public function params() {
        return $this->_params;
    }

    public function param($key = NULL) {
        if (!is_null($key) && isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return FALSE;
    }

    public function redirect($uri) {
        header('Location: '.$uri);
        exit();
    }
}
?>
