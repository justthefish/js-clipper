<?php
class DB {

    //public static $instance; //instance of 
    public $db;
    protected $query;
    public static $models;

    //singleton (?)
    
    public static function me(Model $model) {
        if (!isset(DB::$models[$model->name()->object_name])) {
            $class_name = 'DB_'.ucfirst($model->driver);
            DB::$models[$model->name()->object_name] = new $class_name;
        }

        return DB::$models[$model->name()->object_name];
    }
}
