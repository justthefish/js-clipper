<?php
class DB_Sqlite extends DB {

    public function __construct() {
        $this->db = new PDO(SQLITE_CONNECTION_STRING);

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    }


    //this method if for selects
    //returns dataset
    public function query() {
        $results = $this->db->query($this->query);
        $i = 0;
        $response = array();
        foreach ($results as $result) {
            $response[] = $result;
        }
        if (count($response) > 1) {
            $result = $response;
        }elseif (count($response) == 0) {
            return null;
        } else {
            $result = $response[0];
        }
        $this->query = NULL;
        return $result;
    }

    //this is for update/delete/drop etc, 
    //doesnt return dataset
    public function exec($query = NULL) {
        if (is_null($query)) {
            $query = $this->query;
        }
        $result = $this->db->exec($query);
        $this->query = NULL; //сбрасываем текущий запрос
        return $result;
    }

    public static function shutdown() {
        return $this->db = NULL;
    }

    public function select(Model $model) {

        $this->query = 'select ';

        $this->query .= implode(',', array_keys($model->fields));

        $this->query .= ' from '.$model->table;

        return $this;

    }

    public function where($conditions) {
        $this->query .= ' where '.$conditions;
        return $this;
    }

}
?>
