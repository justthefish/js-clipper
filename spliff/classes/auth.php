<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * auth class
 **/
class Auth {

    private $_salt = 's4mp1e5a1t';
    private $_sessionKey = '_spliff_';
    private $_tokenKey = '_spliff_remember';
    private $_tokenLifetime = 604800;
    protected $_session = NULL;
    protected $logged_in = FALSE;
    private static $instance = NULL;

    public function __construct() {
        $this->_session = Session::me();
    }

    public static function me() {
        if (is_null(self::$instance)) {
            $obj = new Auth();
            self::$instance = $obj;

        } 
        return self::$instance;
        //return Singleton::getInstance(__CLASS__);
    }

    public static function factory() {
        return new Auth();
    }


    public function getUser() {
        return $this->_session->get($this->_sessionKey);
    }

    //функция для использования пароля в явном виде
    public function login ($username, $password, $remember = FALSE) {

        if (empty($password)) return FALSE;
        if (is_string($password)) {
            //клеим пароль с солью
            $password = $this->hash($password);
        }

        return $this->_login($username, $password, $remember);
    }


    public function logout($destroySession = FALSE) {
        if ($destroySession === TRUE) {
            $this->_session->destroy();
        } else {
            $this->_session->delete($this->_sessionKey);
            $this->_session->regenerate();
        }
        return !($this->logged_in);

    }

    public function loggedIn($role = NULL) {
        return NULL !== $this->getUser();
    }

    public function hash($password) {
        return md5($password); //@todo Нужен другой алгоритим, см Redmine
    }

    public function completeLogin($user) {
        $this->_session->regenerate();
        $this->_session->set($this->_sessionKey, $user);
        return TRUE;
    }

    //Для логина по токену (запомнить меня)
    //@todo перенести на уровень выше, тк есть детали реализации
    public function autoLogin() {
        if ($token = (isset($_COOKIE[$this->_tokenKey]) ? 
            $_COOKIE[$this->_tokenKey] : FALSE)) {

                $user = Token::dao()->getByName($token)->getUser();
                if ($user !== NULL) {
                    $this->completeLogin($user);
                    return TRUE;
                }

            }
        return FALSE;
    }

    //Для логина только по юзернейму, без пароля
    //@todo перенести на уровень выше, тк есть детали реализации
    public function forceLogin($username) {
        if (!is_object($username)) {
            $user = new User();
            $user = $user->getByEmail($username);
        }

        if ($user !== NULL) {
            $this->completeLogin($user);
            return TRUE;
        }
        return FALSE;
    }


    //@todo сделать абстрактным и вынести на уровень выше
    //тк содержит детали реализации
    protected function _login($user, $password, $remember)
    {
        if (!is_object($user)) {
            $username = $user;

            $user = new User();
            // Load the user
            $user = $user->getByEmail($username);
            //$users = Criteria::create(User::dao())
            //    ->add(Expression::eq('user', DBValue::create($username)))
            //    ->getList();
            //$user = $users[0];
        }

        if($user !== NULL && $user->password == $password) {
            if ($remember === true) {
                $this->remember($user);
            }
            $this->completeLogin($user);
            return TRUE;
        }

        return FALSE;
    }
    //@todo перенести на уровень выше, тк дктали реализации
    public function remember($user = NULL) {
        //tokens
        if(is_null($user)) {
            $user = $this->getUser();
            $token = Token::create()
                ->setUser($user)
                ->setName(md5(Request::$user_agent))
                ->setExpires(time() + $this->_tokenLifetime);

            Token::dao()->take($token);
            setcookie($this->_tokenKey, $token->getName(), $this->_tokenLifetime);
        }
    }

    //ограничение доступа по ролям (дефолтное)
    //@todo перенести на уровень выше, тк детали реализации
    public function restrict($roles) {

        if (!is_array($roles)) {
            $roles = array($roles);
        }

        if ($this->loggedIn() && in_array($this->getUser()->getRole()->getId(), $roles)) {
            return TRUE;
        }
        return FALSE;
    }

    //ограничение доступа по списку user_id (для запущенных случаев)
    //Для редиректа надо ловить RequestException403 в контроллере
    //@todo перенести на уровень выше, тк детали реализации
    public function restrict_by_user_ids($user_ids) {
        if (!is_array($user_ids)) {
            $user_ids = array($user_ids);
        }

        if ($this->loggedIn() && in_array($this->getUser()->getId(), $user_ids)) {
            return TRUE;
        }

        throw new RequestException403();
        return FALSE;
    }

    
}
