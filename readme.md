JS Clipper
---

Настройки в app/boostrap.php


В конфиге nginx, который будет раздавать js и html в секции location /
нужно указать:

```

    location / {
            ...
            add_header Access-Control-Allow-Origin $http_origin;
            ...
    }
```
Иначе кроссдоменный инклюд работать не будет.

